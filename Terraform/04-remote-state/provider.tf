provider "aws" {
    region = "ap-south-1"
}

resource "aws_instance" "web" {
  ami               = "ami-0aad94d6af90ae48a"
  instance_type     = "t2.nano"
  count             = 3

  tags = {
    Name    = "HelloWorld-${count.index+1}"
    Batch   = "44"
    Trainer = "Manu Verma"
  }
}


terraform {
  backend "s3" {
    bucket = "bat44-dynamodb-remotebackend"
    key    = "sample/terraform.tfstate"
    region = "ap-south-1"
    dynamodb_table = "terraform-locking"
  }
}