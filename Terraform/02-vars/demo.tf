variable "sample-name" {
    default = "Hello and welcome to Terraform"
}

output "sample-op" {
    value = var.sample-name
}

output "sample-op1" {
    value = "value is ${var.sample-name}"
}

# A Variable can be accessed without ${} if only the variable is been called. In case if it's needed to add with some other strings
# then we need to add Double Quotes
# Terraform doesn't support single quotes . . . Only Double Quotes.


# Variable - Data Types 
# terraform Supports data types and are 
# 1) Strings      2) Numbers      3) Booleans

# Always STRINGS should be in double quotes.



variable "number" {
    default = 100 
}

output "number" {
    value = var.number 
}

variable "ex-list" {
    default = [
         "Batch44",
         "DevOps",
         "ManuVerma"
    ]
}

output "ex-list" {
    value = "Welcome to ${var.ex-list[1]} Training ${var.ex-list[0]}, name of the trainer is ${var.ex-list[2]} "
}



######

variable "City" {}
output  "city" {
    value = "Name of the city is ${var.City}"
}


variable "State" {}
output "state" {
    value = "Name of the State is ${var.State}"
}

