variable "sample" {
    default = "Hello Default"
}

output "sample-op" {
    value = var.sample
}

output "sample1" {
    value = "value is ${var.sample}"
}