provider "aws" {
    region = "ap-south-1"
}

terraform {
  backend "s3" {
    bucket = "bat44-dynamodb-remotebackend"
    key    = "sample/terraform.tfstate"
    region = "ap-south-1"
    dynamodb_table = "terraform-locking"
  }
}


module "local-module" {
    source = "./local"
}