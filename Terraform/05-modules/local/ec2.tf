resource "aws_instance" "web" {
  ami               = "ami-0aad94d6af90ae48a"
  instance_type     = "t2.nano"

  tags = {
    Name    = "HelloWorld"
    Batch   = "44"
    Trainer = "Manu Verma"
  }
}