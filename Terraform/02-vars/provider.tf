terraform {
    required_providers {
        prod = {
            source = "hashicorp/aws"
            version = "1.0"
        }
        dev = {
            source = "hashicorp/aws"
            version = "2.0"
        }
    }
}