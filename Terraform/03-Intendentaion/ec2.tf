resource "aws_instance" "web" {
  ami               = "ami-0cda24005eea36f3f"
  instance_type     = "t2.nano"

  tags = {
    Name    = "HelloWorld"
    Batch   = "44"
    Trainer = "Manu Verma"
  }
}

output "public_ip" {
    value =  aws_instance.web.public_ip 
}